import Utils from '../util/Utils';
import DOMUtils from '../util/DOMUtils';
import autocomplete from 'autocompleter';
import telData from '../util/data/telData.json'; 
const EventEmitter = require('events')
import chartStringEvents from '../EventBus/ChartStringEvents';
let chartStringEmitter = chartStringEvents.emitter;

let chartStringMap = new Map(); 

let isDisabled = true;
let alertIsVisible = true;
export default class RecipientGroup {
  constructor(id, config, appDiv){
    this.id = id;
    this.config = config;
    this.number_of_members = 0;
    this.divNode = "";
    this.inputNode = null;
    this.required = !(this.config.minListCount == 0); 
    this.authenticationMethod = null;
    this.defaultValue = config.defaultValue;
    this._appDiv = appDiv;
  }

  addToDOM(parentNode) {
    
    const inputId = 'recipient_' + this.id;

    // Create the div
    var divNode = document.createElement('div');
    divNode.id = "recipient_group_" + this.id;
    divNode.className = "form-group";
    parentNode.appendChild(divNode);
    this.divNode = divNode;
    
    if(!checkForChartField(this.config.label)){
      // Create the label
      var labelNode = document.createElement('label');
      labelNode.innerHTML = this.config.label; 
      labelNode.htmlFor = inputId;
      divNode.appendChild(labelNode);

      var inputNode = document.createElement("input");
      inputNode.id = inputId;
      inputNode.name = inputId;
      inputNode.tabIndex = 0; 
      inputNode.className = 'autocomplete form-control';
      inputNode.placeholder = "Search by Name or Email";

      // inputNode.addEventListener('focusout', () =>{
      //   console.log('test');
      // }); 

      //create auto complete out of text input
      autocomplete({
        onSelect: function(item) {
            inputNode.value = item.value;
            
        },
        input: inputNode,
        minLength: 2,
        emptyMsg: 'Search by Name or Email',
        render: function(item, currentValue) {
            var div = document.createElement("div");
            div.textContent = item.label;
            return div;
        },
        renderGroup: function(groupName, currentValue) {
            var div = document.createElement("div");
            div.textContent = groupName;
            return div;
        },
        //className: 'autocomplete-customizations',
        fetch: function(text, callback) {
              //remove special characters and force value 
            text = text.replace(/[&\#,+()$~%.'":*?`!^<>{}]/g, '').toLowerCase();
            //Filter json data from text input. Return a collection of the filtered employees
            var filteredEmps = telData.filter(function(emp){
              if(emp.m !== "")
              {
                var employeesList = emp.f.toLowerCase() + ' ' + emp.l.toLowerCase() + ' ' + emp.m.toLowerCase();
                return employeesList.includes(text);
              }
            });
            
              //Instantiate empty object. For autocomplete binding purposes
            var employee = {}; 
            //Instantiate an empty array 
            var employeeCollection = []; 
              //loop through filtered array, create a new employee object and add it to the employee collection array.
            filteredEmps.forEach(function(item){
              employee.label = `${item.f} ${item.l} | ${item.m} | ${item.t} | ${item.d}`; 
              employee.value = item.m; 
              employeeCollection.push(employee); 
              employee = {};
            });
            //return employee collection in the auto complete drop down. 
            callback(employeeCollection);
        },
        debounceWaitMs: 200,
        preventSubmit: true,
        disableAutoSelect: true,
        container: document.createElement("div")
      });
      divNode.appendChild(inputNode);
      
      if(this.required) {
        inputNode.required = true;
        labelNode.classList.add("required");
      }

      // If data is not blank, fill it in with predefine information
      if (this.config.defaultValue !== "") {
        inputNode.value = this.config.defaultValue;

        if(!this.config.editable) {
          inputNode.readonly = true;
        }
      }

      if(!checkForChartField(this.config.label)){
        this.inputNode = inputNode;
      }
      
    } else {
      let doaNamesConatiner = document.createElement('div');
        doaNamesConatiner.id = 'recipient_group' + 'doaNames'; 
        doaNamesConatiner.className = 'form-group';
    
        parentNode.appendChild(doaNamesConatiner);
        
        var selectLabel = document.createElement('label');
        selectLabel.innerHTML = modifyChartStringLabel(this.config.label); 
        selectLabel.htmlFor = inputId;
        selectLabel.classList.add("required");
        doaNamesConatiner.appendChild(selectLabel);
  
        let doaNamesSelect = document.createElement('select'); 
        doaNamesSelect.id = 'doa_select';
        doaNamesSelect.className = 'custom-select'; 
        doaNamesSelect.tabIndex = 0; 
        doaNamesSelect.disabled = isDisabled;
        doaNamesSelect.required = true;
        doaNamesSelect.addEventListener('change', () => {
          if(doaNamesSelect.selectedIndex !== 1) {
            toggleAlert(lvlWarning);
          } else {
            removeAlert(lvlWarning);
          }
        })
        chartStringEmitter.on('removeCsApprovers', () => {
          doaNamesSelect.disabled = true;
          chartStringMap.clear();
          removeDropDownOptions(doaNamesSelect);
        })

        chartStringEmitter.on('onGetChartString', (data) => {
          chartStringMap = data;
          createDropDownOptions(doaNamesSelect);
        });

        doaNamesConatiner.appendChild(doaNamesSelect);
        let lvlWarning = document.createElement('div');
        lvlWarning.className = 'alert alert-warning'; 
        let alertHdr = document.createElement('h4');
        alertHdr.textContent = "Warning!";
        lvlWarning.hidden = alertIsVisible;
        lvlWarning.textContent = "You have selected an Approver other than the Level 1 Approver. Are you sure you want to continue?";
        lvlWarning.prepend(alertHdr);
        doaNamesConatiner.appendChild(lvlWarning);
        
        var feedbackNode = document.createElement('div');
        feedbackNode.className = "invalid-feedback";
        feedbackNode.innerText = "Please provide a valid email address"
        doaNamesConatiner.appendChild(feedbackNode);

        this.inputNode = doaNamesSelect;
    }
    
    

    return;
  }

  

  setupValidation(validator) {
    let validationFn = this.runValidation.bind(this);

    let validationTracker = validator.createTracker(this.inputNode, validationFn);

    this.inputNode.addEventListener("change", (event) => {
      this.inputNode.value = this.inputNode.value.trim();
      validationFn(validationTracker, event, false);

    });
  }

  
  runValidation(validationTracker, event, isRevalidate) {
    let error = false;
    let message = null;
    let email = this.inputNode.value;

    if(this.required && email == "") {
      error = true;
      message = `The recipient "${this.config.label}" is required.`
    }
    else if(email != "" && !Utils.isValidEmail(email)) {
      error = true;
      message = `The email "${email}" for recipient "${this.config.label}" is not a valid email address.`
    }

    if(error) {
      this.inputNode.classList.add("is-invalid");
    }
    else {
      DOMUtils.removeClass(this.inputNode, "is-invalid");
    }

    validationTracker.update(error, message);
  }

  getValues() {
    if (this.config.editable) {
      return {
        name: this.config.name,
        recipients: [
          {
            email: this.inputNode.value
          }
        ]
      }
    }
    return null;
  }

  createAdditionalRecipientInput(recipient_id) {
      /***
       * This function add additions recipeints input
       */

      var add_div = document.createElement('div');
      add_div.id = 'add_section_' + this.group_id;
      add_div.className = "add_section";
      this.divNode.appendChild(add_div);

      // Create the add new recipient button
      var add_marker_button = document.createElement("button");
      add_marker_button.type = "button";
      add_marker_button.id = "add_button";

      // Add onclick function to allow us to create new recipient inputs
      add_marker_button.onclick = function () {
          let new_recipient_id = recipient_id + '_' + this.number_of_members;
          this.number_of_members++;
          this.appendNewParticipentInput(new_recipient_id);
      }.bind(this);

      add_div.append(add_marker_button);

      // Add the plus icon to the button
      var add_recipient_marker = document.createElement("i");
      add_recipient_marker.className = "fa fa-plus";

      add_marker_button.appendChild(add_recipient_marker)
  }

  appendNewParticipentInput(participent_id) {
      /***
       * This functiuon appends a new recipient input
       */

      // Create a line break
      var linebreak = document.createElement("br");

      // Create new input field
      var participent_input = document.createElement('input');
      participent_input.type = "text";
      participent_input.className = "form-control";
      participent_input.placeholder = "Enter Recipient's Email";
      participent_input.id = participent_id;
      participent_input.name = participent_id;

      // Append to the div before buttons
      var target = document.getElementById("add_section_" + this.group_id);
      this.divNode.insertBefore(participent_input, target);
  }

  removeParticipentButton() {
      /***
       * This function removes a recipient
       */

      var remove_button = document.createElement("button");
      remove_button.type = "button";
      remove_button.id = "remove_button";
      remove_button.onclick = function () {
          if(this.number_of_members > 0){
              // remove input field
              this.divNode.removeChild(this.divNode.querySelectorAll("input")[this.number_of_members]);
              this.number_of_members--;
          }
      }.bind(this);
      document.getElementById('add_section_' + this.group_id).appendChild(remove_button);

      var remove_button_marker = document.createElement("i");
      remove_button_marker.className = "fa fa-minus";
      remove_button.appendChild(remove_button_marker);
  }
}

const toggleAlert = (warning) => {
  warning.hidden = false;
}

const removeAlert = (warning) => {
  warning.hidden = true;
}
const removeDropDownOptions = (nodeElement) => {
     let options = nodeElement.querySelectorAll('option');
     options.forEach(el => el.remove());
}
const createDropDownOptions = ( nodeElement ) => {
  let option = document.createElement('option');
  option.textContent = "Select an Approver"; 
  nodeElement.appendChild(option);
  for(let [key, obj] of chartStringMap){
      let approverOption = document.createElement('option');
      approverOption.textContent = `${obj.level} - ${obj.name}`; 
      approverOption.value = obj.email;
      nodeElement.appendChild(approverOption); 
      nodeElement.disabled = false; 
  }
}



const checkForChartField = (elementLabel) => {
  let labelArr = elementLabel.split("_");
  if(labelArr.includes("$CFA")) return true;
  return false;
}

const modifyChartStringLabel = (label) => {
  let labelArr = label.split("_"); 
  return labelArr[2];
}

