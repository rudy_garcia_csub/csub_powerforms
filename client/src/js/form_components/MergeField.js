import DOMUtils from '../util/DOMUtils';
import mergeFieldTemplate from 'MergeField.hbs';
import unescape from 'lodash/unescape';
import selectListTemplate from 'SelectList.hbs';

export default class MergeField {
  constructor(config){
    this.config = config;
    this.readonly = !config.editable;
    this._inputNode;
  }

  addToDOM(parentNode) {
    let data = {
      inputId: 'merge_input_' + this.config.fieldName,
      label: unescape(this.config.displayName),
      defaultValue: this.config.defaultValue,
      required: this.config.required,
      readonly: this.readonly,
    };
        //Check is there is an array included in the label
    let labelIsArray = data.label.includes("{");

    let tempDiv = document.createElement('div');
    let div = null; 

    if(labelIsArray) {
        //Add an empty array to the data object
      data.DropDownVals = []; 
        //extract array from label string
      var base = data.label.substring(data.label.lastIndexOf("{") + 1 , data.label.lastIndexOf("}")); 
        //remove white space
      base.replace(/ /g,'');
        //Add values to array
      data.DropDownVals = base.split(';'); 
      //remove array from label
      data.label = data.label.substring(data.label.lastIndexOf(0), data.label.lastIndexOf("{"));
      
      tempDiv.innerHTML = selectListTemplate(data); 
    } else {
      tempDiv.innerHTML = mergeFieldTemplate(data); 
    }

    div = tempDiv.firstChild
    parentNode.appendChild(div);
    // Create the div
    if(labelIsArray){
      this._inputNode = div.querySelector('select');
    } else {
      this._inputNode = div.querySelector('input'); 
    }
  }

  setupValidation(validator) {
    let validationFn = this.runValidation.bind(this);
    let validationTracker = validator.createTracker(this._inputNode, validationFn);

    this._inputNode.addEventListener("change", (event) => {
      validationFn(validationTracker, event, false);
    });
  }

  runValidation(validationTracker, event, isRevalidate) {
    let error = false;
    let message = null;
    let value = this._inputNode.value;

    if(this.config.required && value == "") {
      error = true;
      message = `The field "${this.config.displayName}" is required.`
    }

    if(error) {
      this._inputNode.classList.add("is-invalid");
    }
    else {
      DOMUtils.removeClass(this._inputNode, "is-invalid");
    }

    validationTracker.update(error, message);
  }

  getValues() {
    if (!this.readonly) {
      return {
        fieldName: this.config.fieldName,
        defaultValue: this._inputNode.value
      };
    }
    return null;
  }

}
