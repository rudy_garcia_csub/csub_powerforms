import autocomplete from 'autocompleter';
import chartStringData from '../util/data/ChartStringData.json'; 
import component from '../../hbsTemplates/DOAFilter.hbs';
import DOMUtils from '../util/DOMUtils';
import text from 'body-parser/lib/types/text';
const { EventEmitter } = require("events");
const eventEmitter = new EventEmitter();
import ChartStringQuery from  '../EventBus/ChartStringEvents';
import { filter, uniq } from 'lodash';

let filteredChartString = null;
const AllUnits = new Map(chartStringData.map(csData => {
    return [csData.Business_Unit, `${csData.Business_Unit} - ${csData.Unit_Desc}`]; 
})); 
const AllFunds = new Map(chartStringData.map(csData => {
    return [csData.FUND_CODE, `${csData.FUND_CODE} - ${csData.Fund_desc}`]; 
}));  
const AllDepts = new Map(chartStringData.map(csData => {
    return [csData.DEPTID, `${csData.DEPTID} - ${csData.DeptId_Desc}`]; 
}));  
const AllProj = new Map(chartStringData.map(csData => {
    return [csData.PROJECT_ID,`${csData.PROJECT_ID} - ${csData.Proj_Desc}`]; 
})); 
const AllClass = new Map(chartStringData.map(csData => {
    return [csData.CLASS_FLD,`${csData.CLASS_FLD} - ${csData.Class_Desc}`]; 
})); 
let unitSelected = null; 
let fundSelected = null; 
let deptSelected = null;
let projGrtSelected = null;
let classSelected = null;
let alertIsVisible = true;
let isDisabled = false;
let inputArray = [];
let formContainer = null;
let filterObj = {}; 
const inputIds = {
    unit_input: 'unit_input', 
    fund_input: 'fund_input', 
    dept_input: 'dept_input', 
    proj_input: 'proj_input', 
    class_input: 'class_input'
}; 
export default class DelegationOfAuthorityGruop {
    constructor(id){
        this.id = id; 
        // this.number_of_members = 0; 
        // this.divNode = ""; 
        // this.inputNode = null;  
    }

    addToDom(parentNode) {
            //Main container that holds the form
        formContainer = document.createElement('form');

        let formRow1 = document.createElement('div');
        formRow1.className = "form-row";

        let formGroup1 = document.createElement('div');
        formGroup1.className = 'form-group col-md-6';
        formRow1.appendChild(formGroup1);

        let unitNode = createInputContainerNode('input_container');
        let unitInput = createInput('unit_input');
        unitInput.addEventListener('change', () =>{
            UnitInputProperties(unitInput);
        }); 
        unitInput.addEventListener('input', () => {
            if(unitInput.value.toLowerCase() != unitSelected) {
                unitSelected = null; 
            }
        })
        createAutoCompleter(unitInput,AllUnits); 
        unitNode.appendChild(createLabel('Unit'));
        unitNode.appendChild(unitInput);
        formGroup1.appendChild(unitNode);

        let formGroup2 = document.createElement('div');
        formGroup2.className = "form-group col-md-6"; 

        let fundNode = createInputContainerNode('fund_container');
        let fundInput = createInput('fund_input');
        fundInput.addEventListener('change', () =>{
            FundInputProperties(fundInput);
        });
        fundInput.addEventListener('input', () => {
            if(fundInput.value != fundSelected){
                fundSelected = null;
            }
        })
        createAutoCompleter(fundInput, AllFunds);

        fundNode.appendChild(createLabel('Fund'));
        fundNode.appendChild(fundInput);
        formGroup2.appendChild(fundNode);
        formRow1.appendChild(formGroup2);
        
        let formRow2 = document.createElement('div');
        formRow2.className = "form-row";

        let formGroup3 = document.createElement('div');
        formGroup3.className = "form-group col-md-6"; 
        
        //Department Id
        let deptNode = createInputContainerNode('fund_container');
        let deptInput = createInput('dept_input');
        deptInput.addEventListener('change', () =>{
            DepartmentInputProperties(deptInput); 
        });
        deptInput.addEventListener('input', () => {
            if(deptInput.value != deptSelected){
                deptSelected = null;
            }
        })
        createAutoCompleter(deptInput, AllDepts);
        
        deptNode.appendChild(createLabel('Department'));
        deptNode.appendChild(deptInput);
        formGroup3.appendChild(deptNode);
        formRow2.appendChild(formGroup3);

        let formGroup4 = document.createElement('div');
        formGroup4.className = 'form-group col-md-6';

        let programNode = createInputContainerNode('program_container');

        let programInput = createInput('program_input');
        programNode.appendChild(createLabel('Program'));
        programNode.appendChild(programInput);

        formGroup4.appendChild(programNode);

        formRow2.appendChild(formGroup4);

        let formRow3 = document.createElement('div');
        formRow3.className = "form-row";

        let formGroup5 = document.createElement('div');
        formGroup5.className = "form-group col-md-6";

        let projNode = createInputContainerNode('proj_container');
        let projInput = createInput('proj_input');
        projInput.addEventListener('change', () =>{
            ProjInputProperties(projInput); 
        });
        projInput.addEventListener('input', () => {
            if(projInput.value != projGrtSelected){
                projGrtSelected = null; 
            }
        })
        createAutoCompleter(projInput,AllProj);
        projNode.appendChild(createLabel('Project Id'));
        projNode.appendChild(projInput);
       
        formGroup5.appendChild(projNode);

        formRow3.appendChild(formGroup5);
        
        let formGroup6 = document.createElement('div');
        formGroup6.className = "form-group col-md-6";

        let classNode = createInputContainerNode('class_container');
        let classInput = createInput('class_input');
        classInput.addEventListener('change', () => {
            ClassInputProperties(classInput);
        }); 
        classInput.addEventListener('input', () => {
            if(classInput.value != classSelected){
                classSelected = null;
            }
        })
        createAutoCompleter(classInput,AllClass); 
        classNode.appendChild(createLabel('Class'));
        classNode.appendChild(classInput);

        formGroup6.appendChild(classNode);

        formRow3.appendChild(formGroup6); 

        let formGroup7 = document.createElement('div'); 
        formGroup7.className = 'form-group col-md-6'; 

        
        let formRow4 = document.createElement('div');
        formRow4.className = 'form-row justify-content-end';

        let formGroup8 = document.createElement('div'); 
        formGroup8.className = 'form-group col-md-6';

        let filterBtnNode = document.createElement('div');
        filterBtnNode.id = 'filterBtn_container'; 
        filterBtnNode.className = 'col-md-4 d-flex justify-content-end'; 

        let filterBtn = document.createElement('button');   
        filterBtn.id = 'filterDoa_btn'; 
        filterBtn.className = 'btn btn-primary';
        filterBtn.type = 'button'; 
        filterBtn.innerText = "Search for Approver";
        filterBtn.addEventListener('click', function (){ 
            filterChartString();
            if(filteredChartString.length != 1) {
                toggleAlert(filterWarning);
            } else {
                const csEvent = ChartStringQuery.emitter; 
                csEvent.emit('onSetChartString', filteredChartString);
                toggleInputState(true);
                removeAlert(filterWarning);
            }            
        });

        filterBtnNode.appendChild(filterBtn);
        formGroup8.appendChild(filterBtnNode);
        

        let formGroup9 = document.createElement('div');
        formGroup9.className = 'form-group col-md-6';

        let clearBtnNode = document.createElement('div'); 
        clearBtnNode.id  = 'clear_filters_container'; 
        clearBtnNode.className = 'col-md-4 d-flex justify-content-end';

        let clearBtn = document.createElement('button'); 
        clearBtn.id = 'clearDoa_btn'; 
        clearBtn.className = 'btn btn-outline-danger'; 
        clearBtn.type = 'button'; 
        clearBtn.innerText  = 'Clear Filters'; 
        clearBtn.addEventListener('click', () => {
            removeAlert(filterWarning);
            clearFilters(); 
            toggleInputState(false);
            const csEvent = ChartStringQuery.emitter; 
            csEvent.emit('onClearChartString');
        }); 
        clearBtnNode.appendChild(clearBtn);
        formGroup8.appendChild(clearBtnNode);
        formRow4.appendChild(clearBtnNode);
        formRow4.appendChild(filterBtnNode);

        let formRow5 = document.createElement('div');
        formRow5.className = 'form-row justify-content-center';

        let filterWarning = document.createElement('div');
        filterWarning.className = 'alert alert-warning'; 
        let alertHdr = document.createElement('h4');
        alertHdr.textContent = "Warning!";
        filterWarning.hidden = alertIsVisible;
        filterWarning.textContent = "No approvers found. Please verify filters are correct.";
        filterWarning.prepend(alertHdr);
        formRow5.appendChild(filterWarning);
        formContainer.appendChild(formRow1);
        formContainer.appendChild(formRow2);
        formContainer.appendChild(formRow3);
        formContainer.appendChild(formRow5);
        formContainer.appendChild(formRow4);
        parentNode.appendChild(formContainer);
        this.fundNode = fundNode; 
        this.unitNode = unitNode; 
        return;
        
    }

}

const toggleAlert = (filterWarning) => {
    filterWarning.hidden = false;
}

const removeAlert = (filterWarning) => {
    filterWarning.hidden = true;
}

const createInputContainerNode = (nodeId) => {
     let containerNode = document.createElement('div');
        containerNode.id = `${nodeId}`;
        containerNode.className = "form-group";
        
        return containerNode;
}

const createLabel = (elementLabel) => {
    let label =  document.createElement('label');
    label.textContent = `${elementLabel}`; 
    label.tabIndex = 0; 

    return label;
}

const createInput = (elementId) => {
       let elementNode = document.createElement('input'); 
       elementNode.id = `${elementId}`; 
       elementNode.className = 'form-control'; 
       elementNode.name = 'unitName'; 
       elementNode.tabIndex = 0; 
       elementNode.placeholder = "Enter Text";
       inputArray.push(elementNode);
       return elementNode;
}

const toggleInputState = (isReadOnly) => {
    for(let input of inputArray) {
        input.readOnly = isReadOnly;
    }
}

const clearFilters = () => {
    formContainer.reset();
    filterObj = {}; 
    filteredChartString = null;
    unitSelected = null;
    fundSelected = null;
    deptSelected = null;
    projGrtSelected = null;
    classSelected = null;
}

const filterChartString = () => {
    filterObj.unit = unitSelected ? unitSelected : " ";
    filterObj.fund = fundSelected ? fundSelected : " ";
    filterObj.deptId = deptSelected ? deptSelected : " ";
    filterObj.proj = projGrtSelected ? projGrtSelected : " ";
    filterObj.class = classSelected ? classSelected : " ";
    filterObj.program = " ";
    let filteredData = chartStringData.filter((csObj) => {
        var chartString = csObj.Business_Unit.toLowerCase() === filterObj.unit && 
         csObj.FUND_CODE.toLowerCase() === filterObj.fund && 
         csObj.DEPTID.toLowerCase() === filterObj.deptId && 
         csObj.PROGRAM_CODE.toLowerCase() === filterObj.program && 
         csObj.PROJECT_ID.toLowerCase() === filterObj.proj && 
         csObj.CLASS_FLD.toLowerCase() === filterObj.class; 
        return chartString; 
    }); 
    filteredChartString = filteredData;
    console.log(filteredChartString);
    console.log(filterObj);
}

function getElementValue(elementId, value) {
    switch (elementId) {
        case inputIds.unit_input: 
        unitSelected = value.toLowerCase(); 
        break; 
        case inputIds.fund_input: 
        fundSelected = value.toLowerCase();
        break;
        case inputIds.dept_input: 
        deptSelected = value.toLowerCase();
        break;
        case inputIds.proj_input: 
        projGrtSelected = value.toLowerCase();
        break;
        case inputIds.class_input: 
        classSelected = value.toLowerCase();
        break;
        default: 
    }
}

const filterData = (text, dataSet) => {
    text = text.replace(/[&\#,+()$~%.'":*?`!^<>{}]/g, '').toLowerCase();
    let uniqueArray = []; 
    for(let [key, val] of dataSet){
        let obj = {}; 
         obj.value = key; 
         obj.label = val;
         uniqueArray.push(obj); 
    }
    let filterData = uniqueArray.filter(function(csObj){
        let lst = `${csObj.value.toLowerCase()} ${csObj.label.toLowerCase()}`;
        return lst.includes(text);
    });
    return filterData; 
}

const UnitInputProperties = (input) => {
    let unit = null; 
    if(input.value != "" && unitSelected == null) {
        unit = filterData(input.value, AllUnits); 
        if(unit[0] != null){
            input.value =  `${unit[0].label}` 
            unitSelected = unit[0].value.toLowerCase();
            console.log(unitSelected);
        } else {
            alert('No match found');
            input.value = ""; 
            unitSelected = null;
        }
    }
}

const FundInputProperties = (input) => {
    let fund = null; 
    if(input.value != "" && fundSelected == null) {
        fund = filterData(input.value, AllFunds); 
        if(fund[0] != null){
            input.value =  `${fund[0].label}` 
            fundSelected = fund[0].value.toLowerCase();
            console.log(fundSelected);
        } else {
            alert('No match found');
            input.value = ""; 
            fundSelected = null;
        }
    }
}
const DepartmentInputProperties = (input) => {
    let dept = null; 
    if(input.value != "" && deptSelected == null) {
        dept = filterData(input.value, AllDepts); 
        if(dept[0] != null){
            input.value =  `${dept[0].label}` 
            deptSelected = dept[0].value.toLowerCase();
            console.log(deptSelected);
        } else {
            alert('No match found');
            input.value = ""; 
            deptSelected = null;
        }
    }
}

const  ProjInputProperties = (input) => {
   let project = null; 
    if(input.value != "" && projGrtSelected == null) {
        project = filterData(input.value, AllProj); 
        if(project[0] != null){
            input.value =  `${project[0].label}` 
            projGrtSelected = project[0].value.toLowerCase();
            console.log(projGrtSelected);
        } else {
            alert('No match found');
            input.value = ""; 
            projGrtSelected = null;
        }
    }
}

const ClassInputProperties = (input) => {
    let classObj = null; 
    if(input.value != "" && classSelected == null) {
        classObj = filterData(input.value, AllClass); 
        if(classObj[0] != null){
            input.value =  `${classObj[0].label}` 
            classSelected = classObj[0].value.toLowerCase();
            console.log(classSelected);
        } else {
            alert('No match found');
            input.value = ""; 
            classSelected = null;
        }
    }
}

function createAutoCompleter(element, dataSet) {
    autocomplete({
        onSelect: function(item) {
            element.value = item.label;
            getElementValue(element.id, item.value);    
        },
        input: element,
        minLength: 2,
        emptyMsg: 'Begin by Typing',
        render: function(item, currentValue) {
            var div = document.createElement("div");
            let cbDiv = document.createElement('div');
            cbDiv.className = "form-check"; 
            let checkBox = document.createElement('input'); 
            checkBox.className = 'form-check-input'; 
            checkBox.type = "checkbox"; 
            cbDiv.appendChild(checkBox); 
            let cbLabel = document.createElement('label'); 
            cbLabel.className = "form-check-label"; 
            cbLabel.textContent = item.label;
            cbDiv.appendChild(cbLabel); 
            div.appendChild(cbDiv);
            return div;
        },
        fetch: function(text, callback) {
            let filtered = filterData(text, dataSet);
            callback(filtered);
        },
        debounceWaitMs: 200,
        preventSubmit: true,
        disableAutoSelect: true,
        container: document.createElement("div")
    });
}

