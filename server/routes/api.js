//include modules
const express = require('express');
const fs = require('fs');
const path = require('path');
const multer  = require('multer');
const Axios = require('axios');
const FormData = require('form-data');
const WorkflowConfig = require('../WorkflowConfig.js');
const WorkflowAgreementProcessor = require('../WorkflowAgreementProcessor.js');
const config = require('../../config');
const fetch = require('node-fetch');
const {parse, stringify} = require('flatted');
const { pathToFileURL } = require('url');
const cron = require('node-cron');

//create router and export it
var router = express.Router();
module.exports = router;

//add helpers
const upload = multer({
  dest: path.join(__dirname, '../../temp/uploads/'),
});
const APIClient = Axios.create({
  baseURL: config.adobeApi.url,
  headers: {
    'Access-Token': config.adobeApi.integrationKey
  }
});
 //boomi API call
// const BoomiClient = Axios.create({
//   baseURL: 'http://dockerhost.csub.edu:9091/ws/simple/getDelegationAuth', 
//   auth: {
//     username: 'PowerForms@californiastateuniversity-0VONC2', 
//     password : '1c78b6d3-b4b5-4f0d-a04b-71e363017b3a'
//   }
// });

const BoomiAPIClient = Axios.create({
      baseURL: 'http://dockerhost.csub.edu:9091/ws/simple/getChartString', 
      auth: {
        username: 'PowerForms@californiastateuniversity-0VONC2', 
        password : '1c78b6d3-b4b5-4f0d-a04b-71e363017b3a'
      }, 
      responseType: 'json'
})

// GET /workflows
router.get('/getWorkflows/', async function (req, res, next) {
  const config = req.app.locals.config;

  function getWorkflows() {
    /***
     * This function makes a request to get workflows
     */
    return fetch(config.adobeApi.url + '/workflows', {
      method: 'GET',
      headers: {
        'Access-Token': config.adobeApi.integrationKey
      }
    });
  }

  const workflow_list = await getWorkflows();
  const data = await workflow_list.json();

  res.json(data['userWorkflowList']);
});
//Dell Boomi API call
router.get('/boomiAPI/:params', async function(req, res, next){
  try {
    const queryParams = req.params;
    queryParams.forEach((key, index) => {
      console.log(`${key}: ${queryParams[key]}`)
    })
  }
    // const api_response = await BoomiAPIClient.get(`?${businessUnit=queryParams.Unit}
    //    &fundCode=${queryParams.Fund}&deptId=${queryParams.DeptId}&programCode=${queryParams.programCode}$projId=${queryParams.projGrt}`
    //   ); 
    //   console.log(api_response);
  
  catch (e) {
    console.log(e);
  }
 
});

const getChartStringData = async (req, res, next) => {
  console.log('This is a test');
  try {
    const api_response = await BoomiAPIClient.get().then((response) => {
      writeChartStringData(response.data);
    }); 
  } catch (e) {
    console.error(e);
  } 
}

const writeChartStringData = (data) => {
  try {
    fs.writeFile('./client/src/js/util/data/ChartStringsTest.json', stringify(data) , (err) => {
      console.log('wrote to file');
      if (err){
        throw err;
      } 
  });
  } catch (e) {
    console.log(e);
  }
  
}



// GET /workflows/{workflowId}
router.get('/workflows/:workflowId', async function(req, res, next){
  try {
    const workflowId = req.params.workflowId;
    const api_response = await APIClient.get(`/workflows/${workflowId}`);
    let workflowConfig = new WorkflowConfig(api_response.data, config.WorkFlowConfig);
    res.json(workflowConfig.getClientConfig());
  }
  catch(e) {
    if(e.response) {
      console.error(`API Error: ${e.response.status}`, e.response);
      if(e.response.status == 401) {
        return res.status(500).send();
      }
      else if(e.response.status == 403) {
        return res.status(403).json(e.response.data);
      }
      else if(e.response.status == 404) {
        return res.status(404).json(e.response.data);
      }
    }

    console.error(e);
    res.status(500).send();
  }
});

router.post('/workflows/:workflowId/agreements', async function(req, res, next){
  try {
    const workflowId = req.params.workflowId;
    const wfDataRequest = await APIClient.get(`/workflows/${workflowId}`);
    
    let worflowProcessor = new WorkflowAgreementProcessor(wfDataRequest.data, config.WorkFlowConfig, req.body);
   
    let agreementData = worflowProcessor.getAgreement();
    let sendingAccount = worflowProcessor.getSendingAccount();
    let headers = {};

    if(sendingAccount) {
      headers["x-api-user"] = `email:${sendingAccount}`;
    }
    const agreementRequest = await APIClient.post(`/workflows/${workflowId}/agreements`, agreementData, {headers});
    res.json(agreementRequest.data);
  }
  catch(e) {
    if(e.response) {
      console.error(`API Error: ${e.response.status}`, e.response);
      if(e.response.status == 403) {
        return res.status(403).json(e.response.data);
      }
      else if(e.response.status == 400){
        return res.status(400).json(e.response.data);
      }
      else if(e.response.status == 404) {
        return res.status(404).json(e.response.data);
      }
    }

    console.error(e);
    res.status(500).send();
  }
});

// GET /libraryDocuments/{libraryDocumentId}/documents
router.get('/getLibraryDocuments/:id', async function(req, res, next) {
  const config = req.app.locals.config;

  function getLibraryDocuments() {
    /***
     * This function gets library documents by ID
     */
    return fetch(config.adobeApi.url + "/libraryDocuments/" + req.params.id + "/documents", {
      method: 'GET',
      headers: {
        'Access-Token': config.adobeApi.integrationKey
      }
    });
  }

  const library_document = await getLibraryDocuments();
  const data = await library_document.json();

  es.json(data);
});

// POST /transientDocuments
router.post('/postTransient', upload.single('myfile'), async function (req, res, next) {
  try {
    // Create FormData
    var formData = new FormData();
    formData.append('File-Name', req.file.originalname);
    formData.append('Mime-Type', req.file.mimetype);
    formData.append('File', fs.createReadStream(req.file.path));

    //make request
    const api_response = await APIClient.post("/transientDocuments", formData, {
      headers: {
        'Content-Type': `multipart/form-data;boundary=${formData.getBoundary()}`
      }
    });

    // Delete uploaded doc after transient call
    fs.unlink(req.file.path, function (err) {
      if (err) return console.log(err);
    });

    res.json(api_response.data)
  }
  catch(err) {
    console.error(err);
    if(err.response) {
      console.error(err.response);
      if(err.response.status == 404) {
        res.status(404).json(err.response.data);
        return;
      }
    }
    res.status(500).send();
  }
});
